import React from 'react'
import styled from 'react-emotion'
import { fluidStyle } from 'shiba-ui/lib/utils/utils'

const UploadBtn = styled('button')({
  border: '1px solid black'
})

const buttonStyle = props => ({
  padding: '0.7em',
  color: 'lightblue',
  border: `1px lightblue solid`,
  borderRadius: 10,
  cursor: 'pointer',
  width: 200,
  display: 'block',
  textAlign: 'center',
  fontSize: '0.8em',
  background: 'white',
  margin: 10
})

const Label = styled('label')(buttonStyle as any, fluidStyle)

export const MyButton = styled('button')(buttonStyle as any, fluidStyle)

export default function Upload(props) {
  return (
    <Label>
      Upload File in App
      <input style={{ display: 'none' }} type="file" id="file" {...props} />
    </Label>
  )
}
