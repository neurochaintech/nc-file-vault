import axios from 'axios'
import React, { Component } from 'react'
import { ethDEcrypt } from 'src/services/ethereum'
import { deserialize, downloadBlob } from 'src/services/fileFns'

export default class Download extends Component<any> {
  componentDidMount() {
    console.log(this.props)
    const { hash, name } = this.props.match.params
    const { privateKey } = this.props.identity
    debugger
    axios
      .get('http://localhost:8080/ipfs/' + hash)
      .then(res => res.data)
      .then(data => {
        return ethDEcrypt(privateKey, data)
      })
      .then(data => {
        const myBlob = deserialize(data)
        debugger
        downloadBlob(myBlob, name)
      })
      .catch(console.error)
  }
  render() {
    return <div>Downloading file with hash </div>
  }
}
