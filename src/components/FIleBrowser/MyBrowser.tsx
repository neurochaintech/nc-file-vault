import axios from 'axios'
import Moment from 'moment'
import 'node_modules/react-keyed-file-browser/dist/react-keyed-file-browser.css'
import React, { Component } from 'react'
import styled from 'react-emotion'
import FileBrowser, { Icons } from 'react-keyed-file-browser-ipfs'
import {
  ethDEcrypt,
  ethEncrypt,
  getLastestHash,
  saveInEthereum
} from 'src/services/ethereum'
import { saveFileInIPFS } from 'src/services/ipfs'
import { IFileDescriptor } from 'src/services/models'
import { readFile } from '../../services/fileFns'
import Upload, { MyButton } from '../Upload'

const MyWrapper = styled('div')({ width: 800, margin: 'auto' })

export default class MyBrowser extends Component<any> {
  state = {
    files: [
      // {
      //   key: 'photos/animals/cat in a hat.png',
      //   modified: +Moment().subtract(1, 'hours'),
      //   size: 1.5 * 1024 * 1024
      //   // url: 'http://google.com'
      // }
    ],
    selection: '',
    ipfsHash: ''
  }

  save = () => {
    const {
      identity: { publicKey, privateKey, address }
    } = this.props

    ethEncrypt(publicKey, JSON.stringify(this.state.files))
      .then(encryptedFiles => {
        return saveFileInIPFS(JSON.stringify(encryptedFiles))
      })
      .then(ipfsHash => {
        this.setState({ ipfsHash })
        return saveInEthereum(address, privateKey, ipfsHash)
      })
      .catch(console.error)
  }

  restore = () => {
    axios
      .get('http://localhost:8080/ipfs/' + this.state.ipfsHash)
      .then(res => res.data)
      .then(data => {
        const pk = this.props.identity.privateKey
        return ethDEcrypt(pk, JSON.parse(data))
          .then(JSON.parse)
          .then(files => {
            this.setState({ files })
          })
      })
      .catch(console.error)
  }

  updateLatestHash = () => {
    getLastestHash(this.props.identity.address)
      .then(ipfsHash => {
        this.setState({ ipfsHash })
      })
      .catch(console.error)
  }

  componentDidUpdate = (provProps, prevState) => {
    if (this.state.ipfsHash !== prevState.ipfsHash) {
      this.restore()
    }
  }

  componentDidMount = () => {
    this.updateLatestHash()
  }

  reset = () => {
    this.setState({ files: [] })
  }

  handleCreateFolder = key => {
    this.setState((state: any) => {
      state.files = state.files.concat([
        {
          key
        }
      ])
      return state
    })
  }
  handleCreateFiles = (files, prefix) => {
    this.setState((state: any) => {
      const newFiles = files.map(file => {
        let newKey = prefix
        if (
          prefix !== '' &&
          prefix.substring(prefix.length - 1, prefix.length) !== '/'
        ) {
          newKey += '/'
        }
        newKey += file.name
        return {
          key: newKey,
          size: file.size,
          modified: +Moment()
        }
      })

      const uniqueNewFiles = []
      newFiles.map(newFile => {
        let exists = false
        state.files.map(existingFile => {
          if (existingFile.key === newFile.key) {
            exists = true
          }
        })
        if (!exists) {
          ;(uniqueNewFiles as any).push(newFile)
        }
      })
      state.files = state.files.concat(uniqueNewFiles)
      return state
    })
  }
  handleRenameFolder = (oldKey, newKey) => {
    this.setState((state: any) => {
      const newFiles: any = []
      state.files.map(file => {
        if (file.key.substr(0, oldKey.length) === oldKey) {
          newFiles.push({
            ...file,
            key: file.key.replace(oldKey, newKey),
            modified: +Moment()
          })
        } else {
          newFiles.push(file)
        }
      })
      state.files = newFiles
      return state
    })
  }
  handleRenameFile = (oldKey, newKey) => {
    this.setState((state: any) => {
      const newFiles = []
      state.files.map(file => {
        if (file.key === oldKey) {
          ;(newFiles as any).push({
            ...file,
            key: newKey,
            modified: +Moment()
          })
        } else {
          ;(newFiles as any).push(file)
        }
      })
      state.files = newFiles
      return state
    })
  }
  handleDeleteFolder = folderKey => {
    this.setState((state: any) => {
      const newFiles: any = []
      state.files.map(file => {
        if (file.key.substr(0, folderKey.length) !== folderKey) {
          newFiles.push(file)
        }
      })
      state.files = newFiles
      return state
    })
  }
  handleDeleteFile = fileKey => {
    this.setState((state: any) => {
      const newFiles: any = []
      state.files.map(file => {
        if (file.key !== fileKey) {
          newFiles.push(file)
        }
      })
      state.files = newFiles
      return state
    })
  }

  handleChangeSelection = selection => {
    this.setState({ selection })
  }

  handleFileUpload = event => {
    const file = event.target.files[0]
    const {
      identity: { publicKey }
    } = this.props

    readFile(file).then((fileJson: IFileDescriptor) => {
      console.log('encrypting:', fileJson)

      ethEncrypt(publicKey, fileJson.binaryData)
        .then(result => {
          console.log('encrypted result:', result)
          return result
        })
        .then(saveFileInIPFS)
        .then(fileHash => {
          const newFile = {
            key: this.state.selection + fileJson.name,
            hash: fileHash,
            modified: +Moment(),
            size: fileJson.size
          }

          const { files } = this.state

          this.setState({
            files: [...files, newFile]
          })
        })
        .catch(err => {
          console.log(err)
        })
    })
  }
  render() {
    return (
      <MyWrapper>
        <h3>ipfsHash of file tree: {this.state.ipfsHash}</h3>
        <h3>Selection: {this.state.selection}</h3>
        <FileBrowser
          files={addUrls(this.state.files)}
          icons={Icons.FontAwesome(4)}
          onCreateFolder={this.handleCreateFolder}
          onCreateFiles={this.handleCreateFiles}
          onMoveFolder={this.handleRenameFolder}
          onMoveFile={this.handleRenameFile}
          onRenameFolder={this.handleRenameFolder}
          onRenameFile={this.handleRenameFile}
          onDeleteFolder={this.handleDeleteFolder}
          onDeleteFile={this.handleDeleteFile}
          onChangeSelection={this.handleChangeSelection}
        />
        <Upload onChange={this.handleFileUpload} />
        <MyButton onClick={this.reset}>Reset files in App</MyButton>
        <MyButton onClick={this.save}>Save in IPFS and Eth</MyButton>
        <MyButton onClick={this.restore}>Restore from IPFS and Eth</MyButton>
      </MyWrapper>
    )
  }
}

function addUrls(files) {
  return files.map(file => {
    if (file.hash) {
      const tmp = file.key.split('/')
      const fileName = tmp[tmp.length - 1]
      const param = encodeURI(fileName)

      // const url = '/download/' + file.hash +
      const url = `/download/${file.hash}/${param}`
      file.url = url
    }

    return file
  })
}
