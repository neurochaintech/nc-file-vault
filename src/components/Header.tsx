import React from 'react'
import styled from 'react-emotion'
import { myShadowStyle } from 'shiba-ui'
const logo = require('../assets/logo-neurochain-new.png')

export const Header = styled('header')(props => ({
  height: 80,
  background: props.theme.primary,
  ...myShadowStyle,
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center'
}))

export const Title = styled('h1')({
  // fontSize: '2em',
  // fontFamily: "'Audiowide', cursive",
  margin: '0 15px'
})

export default () => (
  <Header>
    <Title>File Vault</Title>
    <img src={logo} height={50} />
  </Header>
)
