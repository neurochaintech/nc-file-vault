import { injectGlobal } from 'emotion'
/* tslint:disable */
injectGlobal`
    body {
        margin: 0px;
        padding: 0;
        
        font-family: 'Open Sans', sans-serif;
        * {
            box-sizing: border-box;
        }
        color: dimgray;
        background: #f4f8f9;

    };
    html {
        font-size: 16px;
    };
`
/* tslint:enable */
