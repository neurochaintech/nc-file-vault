import { ThemeProvider } from 'emotion-theming'
import * as React from 'react'
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch
} from 'react-router-dom'
import './App.css'
import Download from './components/FIleBrowser/Download'
import MyBrowser from './components/FIleBrowser/MyBrowser'
import Header from './components/Header'
import { IIdentity } from './services/models'
import theme from './styles/theme'

const identity: IIdentity = {
  address: '0xA1161ea105676922fFE21b3c6a588d0b3315355d',
  privateKey:
    '0x51c0301073d23779b728ade34a395c93d1d8446af98e813cb4584475efc95824',
  publicKey:
    'ffeaf0c9966a65acb0c6d2b4fb6f1605aadae73739d289d564ce30eceebb706970eda0c404df8b081424ae08cfdacd6be8e9de496c6839130abd1558bbd64017'
}

class App extends React.Component {
  public render() {
    return (
      <ThemeProvider theme={theme}>
        <>
          <Header />
          <Router>
            <Switch>
              <Route
                path="/browser"
                component={() => <MyBrowser identity={identity} />}
              />
              <Route
                path="/download/:hash/:name"
                component={props => <Download {...props} identity={identity} />}
              />
              <Redirect to="/browser" />
            </Switch>
          </Router>
        </>
      </ThemeProvider>
    )
  }
}

export default App
