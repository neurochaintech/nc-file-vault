import EthCrypto from 'eth-crypto'
const Web3 = require('web3')
import conf from './config'
const etherscanAPI = require('etherscan-api').init(conf.etherScanKey, 'kovan')
const web3 = new Web3(conf.web3Server)

/**
 * Encrypt with Ethereum public key
 */
export function ethEncrypt(publicKey: string, data: string) {
  return EthCrypto.encryptWithPublicKey(
    publicKey, // publicKey
    data // message
  )
}

/**
 * Decrypt with Ethereum private key
 */
export function ethDEcrypt(privateKey: string, data: string) {
  return EthCrypto.decryptWithPrivateKey(privateKey, data)
}

/**
 * Use this to save a hash in Etherum
 */
export const saveInEthereum = (
  address: string,
  privateKey: string,
  hash: string
) =>
  new Promise((resolve, reject) => {
    _getSignature(address, privateKey, hash)
      .then(signed => {
        console.log('Getting receipt...')
        web3.eth
          .sendSignedTransaction(signed.rawTransaction)
          .on('receipt', receipt => {
            console.log('Receipt successfully received!')

            resolve(receipt)
          })
          .on('error', err => {
            reject(err)
          })
      })
      .catch(err => {
        reject(err)
      })
  })

/**
 * Used by saveInEthereum to sign transaction
 */
function _getSignature(address: string, privateKey: string, hash: string) {
  console.log('Getting Ethereum transaction count')
  return Promise.all([
    web3.eth.getTransactionCount(address),
    web3.eth.getGasPrice()
  ])
    .then(result => {
      const tx = {
        // nonce: result[0],
        // nonce: parseInt((new Date().getTime() / 1000) as any), // HACKY NONCE)
        // nonce,
        to: address,
        // gasPrice: web3.eth.gasPrice,
        gas: 100000,
        value: 0,
        data: web3.utils.toHex(hash)
      }
      console.log('Signing transaction count with:', JSON.stringify(tx))
      return web3.eth.accounts.signTransaction(tx, privateKey)
    })
    .catch(err => {
      console.log('Failed getting Ethereum transaction count')
      return err
    })
}

/**
 * Get the data saved in the latest transaction of the given address
 */
export const getLastestHash = (address: string) =>
  etherscanAPI.account
    .txlist(address, null, null, 'desc')
    .then(result => {
      console.log(result)
      const data = result.result[0].input
      console.log(data)
      console.log(Web3.utils.hexToUtf8(data))
      return Web3.utils.hexToUtf8(data)
    })
    .catch(console.error)

/**
 * Create Etheruem identity
 */
export const generateIdentity = () => EthCrypto.createIdentity()
