export interface IIdentity {
  address: string
  privateKey: string
  publicKey: string
}

/**
 * This model describes a serialized file
 */

export interface IFileDescriptor {
  name: string
  binaryData: string
  size: number
}
