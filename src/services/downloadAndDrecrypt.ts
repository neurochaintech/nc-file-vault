import axios from 'axios'
import { ethDEcrypt } from './ethereum'
import { deserialize, downloadBlob } from './fileFns'
import { IFileDescriptor } from './models'

/**
 * Fetch a file from IPFS by it's hash, decrypt it with your prvate Key
 * and download it !
 */

export function downloadAndDecrypt(hash: string, privateKey: string) {
  return axios
    .get('http://localhost:8080/ipfs/' + hash)
    .then(res => res.data)
    .then(data => {
      ethDEcrypt(privateKey, data)
        .then(res => {
          debugger
          const fileDescriptor: IFileDescriptor = JSON.parse(res)

          const file = deserialize(fileDescriptor.binaryData)
          downloadBlob(file, fileDescriptor.name)
        })
        .catch(console.error)
    })
    .catch(console.error)
}
