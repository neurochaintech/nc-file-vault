import { IFileDescriptor } from './models'

/**
 * Transforms a File object into an object of type IFileDescriptor
 * Typically used in our app on a file upload button to tranform
 * the uploaded file into an object of type IFileDescriptor so we
 * can play with it
 */

export function readFile(file: File) {
  const { name, size } = file

  return new Promise(resolve => {
    const reader = new FileReader()
    reader.readAsBinaryString(file)
    reader.onload = () => {
      const binaryData = reader.result as string
      resolve({
        name,
        binaryData,
        size
      } as IFileDescriptor)
    }
  })
}

/**
 * Transform binary data string into a Blob
 */
export function deserialize(binaryDataString: string): Blob {
  const length = binaryDataString.length
  const myUint8Array = new Uint8Array(length)
  for (let i = 0; i < length; i++) {
    myUint8Array[i] = binaryDataString.charCodeAt(i)
  }

  const result = new Blob([myUint8Array], { type: 'application/octet-stream' })

  return result
}

/**
 * Download blob with a given filename
 */
export function downloadBlob(file: Blob, filename: string) {
  if (window.navigator.msSaveOrOpenBlob) {
    // IE10+
    window.navigator.msSaveOrOpenBlob(file, filename)
  } else {
    // Others

    const a = document.createElement('a')
    const url = URL.createObjectURL(file)
    a.href = url
    a.download = filename
    document.body.appendChild(a)
    a.click()
    setTimeout(() => {
      document.body.removeChild(a)
      window.URL.revokeObjectURL(url)
    }, 0)
  }
}
