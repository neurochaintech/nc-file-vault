import ipfsAPI from 'ipfs-api'
import config from './config'

const ipfs = ipfsAPI(config.ipfsServerHost) // leaving out the arguments will default to these values

export async function saveFileInIPFS(binaryData: string) {
  const content = ipfs.types.Buffer.from(JSON.stringify(binaryData))
  const results = await ipfs.files.add(content)
  const hash = results[0].hash // "Qm...WW"
  console.log('uploaded file in IPFS with hash: ', hash)
  return hash
}

export function fetchFilefromIPFS(hash: string) {
  return ipfs.files.get(hash)
}
