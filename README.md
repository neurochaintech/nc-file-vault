# Neurochain File Vault :credit_card:

Save files in IPFS and their hash in Ethereum

## Whats needed

This app runs against an IPFS node and an Ethereum client.

Dummy infura ethereum client and Identity are hardcoded for POC purposes.

You will need to run an IPFS node locally with CORS enabled.

Run `cors-config.sh` to enable CORS on the IPFS node.

## Getting Started

You need to have node and yarn installed locally.
Once you downloaded the project, run `yarn` from the project root in order to download all the required dependencies.

## Use case

**Browsing**

Once you have the app running, you can create directories and add files to a selected directory
via the "Upload file" button. "Reset files in app" will reset your file directory.

To download a file, right click the file name and open in new tab. That will fetch the file from IPFS, decrypt it and download it.

**Saving and restoring from IPFS and Ethereum**

To save the state of your file tree, click on "Save in IPFS and Ethereum".

To restore the state of your file tree, click on "Restore from IPFS and Ethereum".

### How to deploy

Build the project artifact with `yarn build`.
Then, serve in your web server the artifacts that were generated in the build folder.

### Commands

Build front end artifacts in `build` folder

```sh
yarn build
```

Start front end dev server

```sh
yarn start
```

Run tests

```sh
yarn test
```

### Minimals Docs

We have there a POC for saving files (data + name) in Ethereum and IPFS.
The functions that do that work are stored in the `services` folder.
Here is a quick overview of them.

#### IPFS

##### saveFileInIPFS(binaryString)

Saves your binary string in IPFS.

##### fetchFileFromIPFS(fileHash)

Saves your binary string in IPFS.

#### Ethereum

##### ethEncrypt(puclicKey, dataString)

Encrypt data string. Returns an object that contains encrypted data.

##### ethDecrypt(pricateKey, object)

Restore your dataString from the object returned above using your private key.

##### saveInEthereum(address, privateKey, dataString)

Save you datastring in etherum. Returns a promise.

##### getLatestHash(address)

Get the data stored in the latest ethereum transaction for the supplied address.

##### getnerateIdentity()

Create a new ethereum identity.

#### File functions

##### readFile(file)

Use this to handle file uploads on your input.
Returns a primise that resolves to an object of type IFileDescriptor.

##### deserialize(binaryString)

Returns a promise that resolves to a Blob based on the binaryString stuplied.
